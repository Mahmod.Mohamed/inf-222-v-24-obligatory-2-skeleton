import { Environment, newEnv } from "./environment";
import { EvaluateVisitor, EvaluatedResult } from "./evaluateVisitor";
import { parseProgram } from "./parser";
import { Tokenizer } from "./tokenizer";
import { TypeCheckVisitor } from "./typeCheckVisitor";
import { PhysicalUnitEnum } from "./types";

export function interpret(program: string): Environment<EvaluatedResult> {
  let tokenizer = new Tokenizer(program);
  let tokenized = tokenizer.tokenize();
  let parsed = parseProgram(tokenized);

  // 4. Type-check the AST.
  let typeCheckEnv = newEnv<PhysicalUnitEnum>();
  let typeCheckVisitor = new TypeCheckVisitor(typeCheckEnv);
  // Visiting each statement for type checking
  console.log(parsed);
  for (let statement of parsed) {
    typeCheckVisitor.visit(statement); // This will type-check the AST
  }

  // 5. Evaluate the AST.
  let evaluateEnv = newEnv<EvaluatedResult>(); // Initialize environment for values of variables
  let evaluateVisitor = new EvaluateVisitor(evaluateEnv); // Initialize the evaluating visitor
  // Visiting each statement for evaluation
  for (let statement of parsed) {
    evaluateVisitor.visit(statement); // This will evaluate the AST and fill the environment with values
  }

  // 6. Return the environment that stores the values of variables.
  return evaluateEnv; // Return the environment filled with evaluated results
}

