import { Token } from "./tokenizer";
import {
  AstNode,
  Identifier,
  Operator,
  PhysicalUnit,
  MeasuredNumber,
  Assignment,
  Statement,
  PrimitiveExpr,
  PhysicalUnitEnum,
  Distance,
  Time,
  Multiplicative,
  Additive,
  GroupExpr,
  Expr,
  Mass,
  Velocity,
} from "./types";

export function parseProgram(tokens: Token[]): AstNode[] {
  let currentPosition = -1;
  let AstNodes: AstNode[] = [];

  function getCurrentToken() {
    return tokens[currentPosition];
  }

  function advance(): void {
    currentPosition += 1;
  }

  function peek() {
    if (currentPosition + 1 < tokens.length) {
      return tokens[currentPosition + 1];
    } else {
      return null; // Indicates no more tokens ahead
    }
  }

  function error() {
    return new Error(
      "Parsing failed at position: " +
        currentPosition +
        ". The erroneous input token is: " +
        getCurrentToken().value
    );
  }

  
  
  /*** functions for terminal symbols of the grammar ***/

  
  
  function KeywordLet(): void {
    // 1. Peek the next input token.
    // 1.1. If it is the keyword `let`, then:
    if (peek().kind === "KeywordLet") {
      // 1.1.1. Advance the position of the parser.
      advance();
    }
    // 1.2. Otherwise, i.e., if it is not a keyword `let`, report an error.
    else throw error();
  }
  
  function Equals(): void {
    if (peek().kind === "Equals") {
      advance();
    } else {
      throw error();
    }
  }
  
  function Semicolon(): void {
    if (peek().kind === "Separator" && peek().value === ";") {
      advance();
    } else {
      throw error();
    }
  }
  
  function OpeningBracket(): void {
    if (peek().kind === "OpeningBracket") {
      advance();
    } else {
      throw error();
    }
  }
  
  function ClosingBracket(): void {
    if (peek().kind === "ClosingBracket") {
      advance();
    } else {
      throw error();
    }
  }
  


  
  /*** functions for "special" terminal symbols that also have a value (these special terminals are: `NumericalLiteral`, `Identifier` and `PhysicalUnit`) ***/


  
  function NumericalLiteral(): number {
    if (peek().kind === "Number") {
      const token = getCurrentToken(); 
      advance();
      return parseInt(token.value, 10);
    } else {
      throw error();
    }
  }

  function Identifier(): Identifier {
    if (peek().kind === "Identifier") {
      const token = getCurrentToken();
      advance();
      return {
        name: token.value,
        nodeType: "Identifier"
      };
    } else {
      throw error();
    }
  }

  function PhysicalUnit(): PhysicalUnit {
    advance();
    const unit = getCurrentToken();
    if (unit.kind === "PhysicalUnit") {
      let kind: PhysicalUnitEnum;
      const unitValue = unit.value;
      if (["min", "s", "h"].includes(unitValue)) {
        kind = PhysicalUnitEnum.Time;
      } else if (["g", "kg"].includes(unitValue)) {
        kind = PhysicalUnitEnum.Mass;
      } else if (["m", "km"].includes(unitValue)) {
        kind = PhysicalUnitEnum.Distance;
      } else if (["km/h", "km/min", "km/s", "m/h", "m/s", "m/min"].includes(unitValue)) {
        kind = PhysicalUnitEnum.Velocity;
      } else {
        throw error();
      }
      return {
        value: unitValue,
        kind: kind,
        nodeType: "PhysicalUnit"
      };
    } else {
      throw error(); 
    }
  }



  /*** functions for non-terminal symbols of the grammar ***/


  
  function Statement(): Statement {
    // 1.1. A statement is an assignment statement.
    // 1.2. Return a corresponding AST node.
    return AssignStatement();
  }

  function AssignStatement(): Assignment {
    KeywordLet();
    let assignee = Identifier();
    Equals();
    let expr = Expr();
    Semicolon();
    return {
      assignee: assignee,
      expr: expr,
      nodeType: "AssignmentStatement"
    };
  }

  
  function Expr(): Expr {
    return Additive();
  }

  function GroupExpr(): GroupExpr {
    OpeningBracket();
    let expr = Expr();
    ClosingBracket();
    return {
      subExpr: expr,
      nodeType: "GroupExpr"
    };
  }

  function PrimitiveExpr(): PrimitiveExpr {
    switch (peek().kind) {
      case "Identifier":
        // Handles use of a variable.
        return Identifier();
      case "Number":
        // Handles a measured number, i.e., a number with a physical unit.
        return MeasuredNumber();
      case "OpeningBracket":
        // Handles a group expression, i.e., an expression in parentheses.
        return GroupExpr();
      default:
        throw error();
    }
  }
  

  function MeasuredNumber(): MeasuredNumber {
    if (peek().kind === "Number") {
      let numericalValue = parseFloat(getCurrentToken().value);
      advance(); 
      
      let unit = PhysicalUnit(); // Consumes the unit token
      
      return {
        numericalValue,
        unit,
        nodeType: "MeasuredNumber"
      };
    } else {
      throw error();
    }
  }
  
  function OpAddSub(): Operator {
    if (peek().kind === "OpAddSub") {
      advance();
    } else {
      throw error();
    }
    switch (getCurrentToken().value) {
      case "+":
        return {
          value: "+",
          nodeType: "OpAddSub"
        };
      case "-":
        return {
          value: "-",
          nodeType: "OpAddSub"
        };
      default:
        throw error();
    }
  }
  

  function OpMulDiv(): Operator {
    if (peek().kind === "OpMulDiv") {
      advance();
    } else {
      throw error();
    }
    switch (getCurrentToken().value) {
      case "*":
        return {
          value: "*",
          nodeType: "OpMulDiv"
        };
      case "/":
        return {
          value: "/",
          nodeType: "OpMulDiv"
        };
      default:
        throw error();
    }
  }

  function Additive(): PrimitiveExpr | Multiplicative | Additive {
    let left: PrimitiveExpr | Multiplicative | Additive = Multiplicative();
    while (peek().kind === "OpAddSub") {
      let op = OpAddSub();
      let right = Multiplicative();
      left = {
        left,
        op,
        right,
        nodeType: "Additive",
      };
    }
    return left;
  }
  

  function Multiplicative(): Multiplicative | PrimitiveExpr {
    let left: PrimitiveExpr | Multiplicative = PrimitiveExpr();
    while (peek().kind === "OpMulDiv") {
      let op = OpMulDiv();
      let right = PrimitiveExpr();
      left = {
        left,
        op,
        right,
        
        nodeType: "Multiplicative",
      };
    }
    return left;
  }
  

}
