import { AbstractVisitor } from "./abstractVisitor";
import { Environment } from "./environment";
import {
  Assignment,
  Additive,
  Identifier,
  Multiplicative,
  MeasuredNumber,
  AstNode,
  PhysicalUnitEnum,
  GroupExpr,
} from "./types";

export class TypeCheckVisitor implements AbstractVisitor {
  
  constructor(private env: Environment<PhysicalUnitEnum>) { }
  
  visit(node: AstNode): PhysicalUnitEnum {
    // 1. Depending on the type of the AST node:
    switch (node.nodeType) {

      // 1.1. If it is a measured number, then:
      case "MeasuredNumber":
        // 1.1.1. Cast the node to type `MeasuredNumber`.
        node = node as MeasuredNumber;
        // 1.1.2. Return the physical unit of the node.
        return node.unit.kind;


      case "GroupExpr":
        node = node as GroupExpr;
        return this.visit(node.subExpr);

      case "AssignmentStatement":
        node = node as Assignment;
        let exprType = this.visit(node.expr); 
        let fromEnv = this.env.lookup(node.assignee.name, this.env);

        if (fromEnv !== undefined && fromEnv !== exprType) {
          // If types don't match, throw an error
          throw new Error(`Type mismatch for variable '${node.assignee.name}'. Expected ${assigneeType}, found ${exprType}.`);
        } else {
          this.env.declare(node.assignee.name, exprType, this.env);
          return exprType;
        }


      // 1.4. If it is an identifier, then:
      case "Identifier":
        // 1.4.1. Cast the node to type `Identifier`.
        node = node as Identifier;
        // 1.4.2. Lookup the type (i.e., its physical unit) of the variable in the environment.
        let type = this.env.lookup(node.name, this.env)
        // 1.4.3. If the looked up type is not undefined, then:
        if (type !== undefined) {
          // 1.4.3.1. Return the looked up type.
          return type;
        } else {
          // 1.4.4. Otherwise, i.e., if the looked up type is undefined, then report an error.
          throw new Error("Variable " + node.name + " is not defined");
        }

      // 1.5. If it is an addition-like expression, then:
      // 1.5. If it is an addition-like expression, then:
      case "Additive": {
        node = node as Additive;

        // Type check the left and right children of the node
        const leftType = this.visit(node.left);
        const rightType = this.visit(node.right);

        // 1.5.4. Depending on the operation `op` of the node:
        switch (node.op.value) {
          // 1.5.4.1. If it is `+`, then:
          case "+":
            // Addition requires both operands to have the same type
            if (leftType === rightType) {
              // Return the common type of both operands
              return leftType;
            } else {
              // Report an error if the types of the operands do not match
              throw new Error(`Type mismatch for addition: cannot add ${leftType} to ${rightType}`);
            }

          // 1.5.4.2. If it is `-`, then:
          case "-":
            // Subtraction, like addition, requires both operands to have the same type
            if (leftType === rightType) {
              // Return the common type of both operands
              return leftType;
            } else {
              // Report an error if the types of the operands do not match
              throw new Error(`Type mismatch for subtraction: cannot subtract ${rightType} from ${leftType}`);
            }

          // 1.5.4.3. Otherwise, report an error for an unsupported operator.
          default:
            throw new Error(`Unsupported operator ${node.op.value} in addition-like expression`);
        }
      }


      // 1.6. It it is a multiplication-like expression, then:
      case "Multiplicative": {
        node = node as Multiplicative;

        const leftType = this.visit(node.left);
        const rightType = this.visit(node.right);

        if (node.op.value === "*") {
          if (leftType === rightType) {
            return leftType;
          } else {
            throw new Error(
              "Incompatible types, " + leftType + " cannot be multiplied with " + rightType
            );
          }
        } else if (node.op.value === "/") {
          if (leftType === PhysicalUnitEnum.Distance && rightType === PhysicalUnitEnum.Time) {
            return PhysicalUnitEnum.Velocity;
          } else {
            throw new Error("Incompatible types: cannot divide units of " + leftType + " by " + rightType);
          }
        }
        throw new Error("Failure!");
      }


      // 1.7. Otherwise:
      default:
        // 1.7.1. Report an error.
        throw new Error("System failure, How did you do this?");
    }
  }
}
