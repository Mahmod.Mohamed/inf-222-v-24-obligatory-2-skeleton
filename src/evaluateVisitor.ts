import { AbstractVisitor } from "./abstractVisitor";
import { Environment } from "./environment";
import {
  Assignment,
  Additive,
  Identifier,
  Multiplicative,
  MeasuredNumber,
  AstNode,
  PhysicalUnitEnum,
  PhysicalUnit,
  GroupExpr,
} from "./types";

export interface EvaluatedResult {
  value: number;
  unit: PhysicalUnit;
}

export class EvaluateVisitor implements AbstractVisitor {
  
  constructor(private env: Environment<EvaluatedResult>) { }

  visit(node: AstNode): EvaluatedResult {
    // 1. Depending on the type of the AST node:
    switch (node.nodeType) {
        
      case "AssignmentStatement":
        node = node as Assignment;
        let expr = this.visit(node.expr);
        this.env.declare(node.assignee.name, expr, this.env);
        return expr;

      case "Identifier":
        node = node as Identifier;
        let lookedUp = this.env.lookup(node.name, this.env);
        if (lookedUp !== undefined) {
          return lookedUp;
        }
        throw new Error("Variable " + node.name + "not in environment");

      // 1.3. If it is a group expression, then:
      case "GroupExpr":
        node = node as GroupExpr;
        return this.visit(node.subExpr);

      // 1.4. If it is a measured number, then:
      case "MeasuredNumber":
        node = node as MeasuredNumber;
        switch (node.unit.value) {
          case "g":
            return {
              value: node.numericalValue,
              unit: { ...node.unit, value: "g" }
            };
          case "kg":
            return {
              value: node.numericalValue * 1000,
              unit: { ...node.unit, value: "g" }
            };
          case "h":
            return {
              value: node.numericalValue * 3600,
              unit: { ...node.unit, value: "s" }
            };
          case "m":
            return {
              value: node.numericalValue,
              unit: { ...node.unit, value: "m" }
            };
          case "km":
            return {
              value: node.numericalValue * 1000,
              unit: { ...node.unit, value: "m" }
            };
          case "s":
            return {
              value: node.numericalValue,
              unit: { ...node.unit, value: "s" }
            };
          case "min":
            return {
              value: node.numericalValue * 60,
              unit: { ...node.unit, value: "s" }
            };
          case "m/s":
            return {
              value: node.numericalValue,
              unit: { ...node.unit, value: "m/s" }
            };
          case "m/min":
            return {
              value: node.numericalValue * 0.01667,
              unit: { ...node.unit, value: "m/s" }
            };
          case "m/h":
            return {
              value: node.numericalValue * 0.002778,
              unit: { ...node.unit, value: "m/s" }
            };
          case "km/s":
            return {
              value: node.numericalValue * 1000,
              unit: { ...node.unit, value: "m/s" }
            };
          case "km/min":
            return {
              value: node.numericalValue * 16.667,
              unit: { ...node.unit, value: "m/s" }
            };
          case "km/h":
            return {
              value: node.numericalValue * 0.2778,
              unit: { ...node.unit, value: "m/s" }
            };
          default:
            throw new Error("Invalid unit used!");
        }


      // 1.5. If it is an addition-like expression, then:
      case "Additive":
        //
        node = node as Additive;
        
        let additiveLeft = this.visit(node.left);
        
        let additiveRight = this.visit(node.right);
        
        if (node.op.value === "+") {
          return {
            value: additiveLeft.value + additiveRight.value,
            unit: additiveLeft.unit,
          };
        } else if (node.op.value === "-") {
          return {
            value: additiveLeft.value - additiveRight.value,
            unit: additiveLeft.unit,
          };
        }
        else {
          throw new Error("System failure");
        }

      // 1.6. If it is a multiplication-like expression, then:
      case "Multiplicative":
        node = node as Multiplicative;
        let multiplicativeLeft = this.visit(node.left);
        let multiplicativeRight = this.visit(node.right);

        if (node.op.value === "*") {
          
          return {
            value: multiplicativeLeft.value * multiplicativeRight.value,
            unit: multiplicativeLeft.unit,
          };
        } else if (node.op.value === "/") {
          if (multiplicativeRight.value !== 0) {
            
            return {
              value: multiplicativeLeft.value / multiplicativeRight.value,
              unit: multiplicativeLeft.unit,
            };
          }
          else {
            throw new Error("Cannot divide by 0");
          }
        }
        else {
          throw new Error("System failure");
        }

      // 1.7. Otherwise:
      default:
        // 1.7.1. Report an error.
        throw new Error("System failure, How did you do this?");
    }
  }
}
